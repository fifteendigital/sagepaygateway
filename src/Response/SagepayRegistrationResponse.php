<?php

namespace Fifteen\SagepayGateway\Response;

use Psr\Http\Message\ResponseInterface;

class SagepayRegistrationResponse
{
    protected $headers;
    protected $body;

    /**
     * @param ResponseInterface $response   SagePay's response to the initial purchase request
     */
    public function __construct(ResponseInterface $response)
    {
        $this->decode($response);
    }

    /**
     * Parse the response into an array of headers and body values
     *
     * @param ResponseInterface $response
     */
    protected function decode($response)
    {
        foreach ($response->getHeaders() as $key => $value) {
            $this->headers[$key] = $value;
        }

        $lines = explode("\n", $response->getBody()->getContents());
        $data = [];

        foreach($lines as $line) {
            $line = explode('=', $line, 2);
            if (!empty($line[0])) {
                $data[trim($line[0])] = isset($line[1]) ? trim($line[1]) : '';
            }
        }

        // For some reason, on OK REPEATED registrations, SagePay returns the VPSTxId without wrapping braces,
        // however the md5 hash calculation requires them. So, for simplicity and consistency, they are added here
        // if not already present
        if (isset($data['VPSTxId'])) {
            if (substr($data['VPSTxId'], 0, 1) != '{') {
                $data['VPSTxId'] = '{' . $data['VPSTxId'];
            }
            if (substr($data['VPSTxId'], strlen($data['VPSTxId']) - 1, 1) != '}') {
                $data['VPSTxId'] = $data['VPSTxId'] . '}';
            }
        }

        $this->body = $data;
    }

    /**
     * @return bool     True if the registration succeeded
     */
    public function isSuccessful()
    {
        return $this->body['Status'] === 'OK' || $this->body['Status'] === 'OK REPEATED';
    }

    /**
     * @return string   Returns the external SagePay redirect URL if the registration was successful
     */
    public function redirectUrl()
    {
        if (isset($this->body['NextURL'])) {
            return  $this->body['NextURL'];
        } else {
            return '';
        }
    }

    /**
     * @return string   Concatenates status and reason for status
     */
    public function getStatus()
    {
        if (isset($this->body['Status']) && isset($this->body['StatusDetail'])) {
            return $this->body['Status'] . ' ' . $this->body['StatusDetail'];
        } else {
            return '';
        }
    }

    /**
     * @return array    The whole body of the registration. It is recommended that you store this for later reference
     */
    public function getTransactionReference()
    {
        return $this->body;
    }
}