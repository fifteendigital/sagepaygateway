<?php

namespace Fifteen\SagepayGateway\Response;

class SagepayConfirmationResponse
{
    protected $status = '';
    protected $status_detail = '';
    /**
     * @var bool    Success of the transaction (i.e. if the customer has paid)
     */
    protected $success = false;
    /**
     * @var string  The redirect URL back to your site
     */
    protected $url = '';

    /**
     * @var string  Redirect target for successful transactions
     */
    protected $success_url;

    /**
     * @var string  Redirect target for unsuccessful transactions
     */
    protected $failure_url;

    /**
     * @var array   Fields that may not be present in a confirmation, but may be required to compute VPSSignature
     */
    protected $optional_md5_fields = [
        'TxAuthNo',
        'AVSCV2',
        'AddressResult',
        'PostCodeResult',
        'CV2Result',
        '3DSecureStatus',
        'CAVV',
        'AddressStatus',    // PayPal only
        'PayerStatus',      // PayPal only
        'CardType',
        'Last4Digits',
        'Surcharge',
        'DeclineCode',
        'ExpiryDate',
        'FraudResponse',
        'BankAuthCode',
        'Token',
    ];

    /**
     * @var array   Error messages for all known failure conditions
     */
    protected $payment_rejection_messages = [
        'ABORT'         => 'The user aborted this transaction, or was inactive for more than 15 minutes.',
        'NOTAUTHED'     => 'The bank did not authorise this transaction.',
        'REJECTED'      => 'Vendor fraud screening rules were not met for this transaction, so it was rejected.',
        'ERROR'         => 'SagePay returned an error status. This is usually indicative of bank connectivity problems.',
        'PENDING'       => 'European Payments are not currently supported. Ensure all European Payments are disabled
                            in MySagePay.',
        'MISMATCH'      => 'Data in the confirmation from SagePay did not match the original registration details. It is 
                            possible that you have pulled the wrong order data, or that an error occured in transit.',
        'SIGNATUREFAIL' => 'The supplied confirmation signature did not match the computed value. It is possible that 
                            there was an attempt to tamper with the transaction, or an error in transit.',
    ];

    /**
     * @param string    $vendor         Vendor name, used in VPSSignature validation
     * @param array     $confirmation   Confirmation details returned by SagePay
     * @param array     $registration   Registration details confirmed previously
     * @param string    $success        URL for successful, cleared transactions
     * @param string    $failure        URL for failed transactions
     */
    public function __construct($vendor, $confirmation, $registration, $success, $failure)
    {
        $this->success_url = $success;
        $this->failure_url = $failure;
        $this->validate($vendor, $confirmation, $registration);
    }

    /**
     * @return bool     Success status of the transaction (i.e. if the customer has paid)
     */
    public function isSuccessful()
    {
        return $this->success;
    }

    /**
     * @return string   Response body to return to SagePay
     */
    public function getResponse()
    {
        return
            "Status=" . $this->status . "\n" .
            "StatusDetail=" . $this->status_detail . "\n" .
            "RedirectURL=" . $this->url;
    }

    /**
     * Validates the confirmation request from SagePay and determines components of the response
     *
     * @param  string   $vendor         Vendor name
     * @param  array    $conf_data      Confirmation details returned by SagePay
     * @param  array    $reg_data       Registration details confirmed previously
     * @return bool                     Success of the validation (_NOT_ success of the transaction)
     */
    protected function validate($vendor, $conf_data, $reg_data)
    {
        switch ($conf_data['Status']) {

            case 'ABORT':
                $this->log($conf_data['VendorTxCode'], 'ABORT');
                break;

            case 'NOTAUTHED':
                $this->log($conf_data['VendorTxCode'], 'NOTAUTHED');
                break;

            case 'REJECTED':
                $this->log($conf_data['VendorTxCode'], 'REJECTED');
                break;

            // European Payments are not currently supported neatly, as they are only cleared on a weekly basis.
            // If you report success, the vendor may think the payment has cleared if they don't check MySagePay, but
            // if you report failure, the payment will still be submitted, but the customer will be told there was a
            // problem. Currently, we return 'INVALID' so that SagePay cancels the transaction on their end.
            // If 'PENDING' or 'ERROR' is received, immediately return failure, don't bother computing VPSSignature.
            case 'PENDING':
                $this->log($conf_data['VendorTxCode'], 'PENDING');

                $this->status = 'INVALID';
                $this->status_detail = 'European Payments should not be enabled';
                $this->url = $this->failure_url;

                return false;

            case 'ERROR':
                $this->log($conf_data['VendorTxCode'], 'ERROR');

                $this->status = 'ERROR';
                $this->status_detail = 'Received an ERROR response from SagePay';
                $this->url = $this->failure_url;

                return false;
        }

        // Ensure we are confirming the correct transaction. Immediately abort if not.
        foreach(['VPSTxId'] as $key) {
            if ($conf_data[$key] != $reg_data[$key]) {

                $this->log($conf_data['VendorTxCode'], 'MISMATCH');

                $this->status = 'INVALID';
                $this->status_detail = 'Confirmation data received did not match stored registration data';
                $this->url = $this->failure_url;

                return false;
            }
        }

        // Some values needed to compute VPSSignature are not always present in the confirmation
        foreach ($this->optional_md5_fields as $field) {
            if (empty($conf_data[$field])) {
                $conf_data[$field] = '';
            }
        }

        // Re-compute VPSSignature to ensure a match
        $base = $conf_data['VPSTxId'] . $conf_data['VendorTxCode'] . $conf_data['Status'] . $conf_data['TxAuthNo'] .
            $vendor . $conf_data['AVSCV2'] . $reg_data['SecurityKey'] . $conf_data['AddressResult'] .
            $conf_data['PostCodeResult'] . $conf_data['CV2Result'] . $conf_data['GiftAid'] .
            $conf_data['3DSecureStatus'] . $conf_data['CAVV'] . $conf_data['AddressStatus'] . $conf_data['PayerStatus'] .
            $conf_data['CardType'] . $conf_data['Last4Digits'] . $conf_data['DeclineCode'] . $conf_data['ExpiryDate'] .
            $conf_data['FraudResponse'] . $conf_data['BankAuthCode'];

        if (strtoupper(md5($base)) != $conf_data['VPSSignature']) {
            $this->log($conf_data['VendorTxCode'], 'SIGNATUREFAIL');
            $this->mapStatus($conf_data['Status'], false);
        } else {
            $this->mapStatus($conf_data['Status'], true);
            if ($conf_data['Status'] == 'OK') $this->success = true;
        }

        return true;
    }

    /**
     * Map a status and signature verification state onto response parameters
     *
     * @param  string   $status                 The status received from SagePay
     * @param  bool     $signature_verified     VPSSignature verification status
     */
    protected function mapStatus($status, $signature_verified)
    {
        $this->status = $signature_verified ? 'OK' : 'INVALID';
        $this->status_detail = $signature_verified ?
            'Validation completed successfully' : 'VPSSignature did not match the expected value';

        switch ($status) {
            case 'OK':
                $this->url = $signature_verified ? $this->success_url : $this->failure_url;
                break;
            case 'ABORT':
            case 'NOTAUTHED':
            case 'REJECTED':
            default:
                $this->url = $this->failure_url;
                break;
        }
    }

    /**
     * Logs transaction failure reason to the standard PHP error log. May help in debugging failed transactions
     *
     * @param string $transaction_id
     * @param string $status
     */
    protected function log($transaction_id, $status)
    {
        if (isset($this->payment_rejection_messages[$status])) {
            error_log("VendorTxCode: " . $transaction_id . " - " . $this->payment_rejection_messages[$status]);
        } else {
            error_log("VendorTxCode: " . $transaction_id . " - An unknown confirmation status was received from SagePay");
        }
    }
}