<?php

namespace Fifteen\SagepayGateway\Request;

use Fifteen\SagepayGateway\Response\SagepayRegistrationResponse;
use GuzzleHttp\Client;

class SagepayPurchaseRequest
{
    protected $client;
    /**
     * @var string      Transaction type (payment, authorise, defer, etc.)
     */
    protected $action = 'PAYMENT';
    /**
     * @var string      SagePay endpoint to begin processing the transaction
     */
    protected $end_point;
    /**
     * @var array       Request body to send to SagePay
     */
    protected $payload;

    /**
     * @param string    $vendor         Vendor name
     * @param string    $callback       Notification URL that SagePay will POST transaction confirmation to
     * @param array     $purchase_data  Data about the order being placed (address, etc.)
     * @param bool      $test           Whether or not this is a test mode transaction
     */
    public function __construct($vendor, $callback, $purchase_data, $test)
    {
        $this->client = new Client();
        $this->vendor = $vendor;
        $this->end_point = $test ?
            'https://test.sagepay.com/gateway/service/vspserver-register.vsp' :
            'https://live.sagepay.com/gateway/service/vspserver-register.vsp';

        $this->payload = $purchase_data + ['Vendor' => $vendor, 'TxType' => $this->action, 'NotificationURL' => $callback];
    }

    /**
     * POST the request to SagePay, build and return the response
     *
     * @return SagepayRegistrationResponse
     */
    public function send()
    {
        return new SagepayRegistrationResponse($this->client->post(
            $this->end_point,
            ['form_params' => $this->payload]
        ));
    }
}