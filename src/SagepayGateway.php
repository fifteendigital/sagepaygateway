<?php

namespace Fifteen\SagepayGateway;

use Fifteen\SagepayGateway\Request\SagepayPurchaseRequest;
use Fifteen\SagepayGateway\Response\SagepayConfirmationResponse;
use Fifteen\SagepayGateway\Response\SagepayRegistrationResponse;

class SagepayGateway
{
    protected $vendor;

    /**
     * @param  string   $vendor             SagePay vendor name
     * @param  bool     $test_mode          Whether to use the test or live SagePay server
     */
    public function __construct($vendor, $test_mode = false)
    {
        $this->vendor = $vendor;
        $this->test_mode = $test_mode;
    }

    /**
     * Request a new purchase through SagePay. The response is returned with all received data, so that any extra
     * processing you might need can be done before redirecting out to SagePay
     *
     * @param  array    $purchase_data      Information about the purchase that SagePay requires (address, etc.)
     * @param  string   $callback           NotificationURL on your server that SagePay will call back to later
     * @return SagepayRegistrationResponse  Encapsulated response from SagePay
     */
    public function purchase($purchase_data, $callback)
    {
        return (new SagepayPurchaseRequest($this->vendor, $callback, $purchase_data, $this->test_mode))->send();
    }

    /**
     * Verify transaction confirmation details received from SagePay and confirm whether the details are valid.
     * The returned response object allows checking the success of the transaction, and contains the response body to
     * return to SagePay in order to accept the transaction
     *
     * @param  array    $confirmation       Confirmation details returned by SagePay
     * @param  array    $registration       Registration details confirmed following purchase request
     * @param  string   $success            URL for successful, cleared transactions
     * @param  string   $failure            URL for failed transactions
     * @return SagepayConfirmationResponse  Encapsulated response to send back to SagePay
     */
    public function confirm($confirmation, $registration, $success, $failure)
    {
        return new SagepayConfirmationResponse($this->vendor, $confirmation, $registration, $success, $failure);
    }
}