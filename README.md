SagepayGateway
==============

An easy-to-use server implementation for SagePay purchase
---------------------------------------------------------

This package will allow you to quickly get up and running with SagePay purchasing using the server integration method.
It should also be relatively easy to add in refunds, voids, etc. with minimal work. There shouldn't be anything that
relies on Laravel that I can think of, its only dependency is Guzzle.

Usage
-----

Create a new instance of `SagepayGateway` using your vendor name and an (optional) test mode flag (default live):

`$gateway = new Fifteen\SagepayGateway\SagepayGateway('vendor_name', true)`

To register a purchase with SagePay, call `purchase()` with the purchase data and your notification URL. Purchase data
is an associative array of all the mandatory fields and any additional optional fields ([see SagePay documentation](http://www.sagepay.co.uk/file/25046/download-document/SERVER_Integration_and_Protocol_Guidelines_270815.pdf))
The notification URL is the end-point on your site that SagePay POSTs to with the results of the transaction:

`$response = $gateway->purchase($purchase_data, 'http://myshop.co.uk/api/sagepay-notification');`

If you passed valid data, this will return a `SagepayRegistrationResponse` object containing the status of the request, 
the SagePay unique VPS transaction ID, a security key (you should store these, with their keys as supplied by SagePay-an
included method `$response->getTransactionReference()` will do this for you), and a URL to the SagePay entry-point to 
begin the transaction. To proceed, just redirect to that URL, accessed with `$response->getRedirectUrl()`.

Whether the transaction completes successfully, is aborted, or fails validation, your notification URL will receive a 
POST with the results of the transaction. To manage this, use `SagepayGateway's confirm()` method. This takes four
parameters; an associative array of all the data received in the POST, an array of the data you received in the 
registration stage, a URL for if the purchase is successful, and a URL for if it fails:

`$response = $gateway->confirm(Request::all(), $order->registrationDetails(), route('success'), route('fail'))`

This will verify the signature of the confirmation, determine whether the data received is valid, and formulate a response.
It returns a `SagepayConfirmationResponse` object. You can use `$response->isSuccessful()` if you want to do any further 
processing before responding to SagePay. When you want to respond, use `$response->getResponse()` to get the body, send
that to SagePay, and they will redirect the customer back to either your success or failure pages. 

Known Issues
------------

This package doesn't play nicely with European Payments that return the 'PENDING' status in the confirmation. This is 
because it becomes unclear whether the transaction should be considered a success or not - if yes the vendor risks being
out of pocket, if not then the customer will believe the payment has failed then get charged a few days later when it 
clears. The current solution is to respond 'INVALID' when confirming a 'PENDING' transaction, which causes SagePay to
cancel the transaction at their end.

By default, any failed transactions will output a VendorTxCode and a failure reason to the configured PHP error log
location, to aid in debugging failed payments. If you consider this a security risk in your project, there is currently
no way to disable it short of commenting out the body of `SagepayConfirmationResponse->log()`.